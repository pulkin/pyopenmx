#!/usr/bin/env python3
from setuptools import setup

setup(
    name='pyopenmx',
    version='0.0',
    author='pulkin',
    description='OpenMX in python',
    long_description=open('README.md').read(),
    packages=['pyopenmx'],
    test_suite="nose.collector",
    tests_require="nose",
)

