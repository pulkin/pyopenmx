[![Build status](https://gitlab.kwant-project.org/pulkin/pyopenmx/badges/master/pipeline.svg)](https://gitlab.kwant-project.org/pulkin/pyopenmx/pipelines)

pyopenmx
========

Computations using python and [OpenMX](http://openmx-square.org).

