from unittest import TestCase
from pathlib import Path
from numpy import testing

from .parsers import parse_basis_spec, parse_basis_spec_conventional, PAOParser


class TestParsers(TestCase):
    def test_basis_string(self):
        self.assertEqual(parse_basis_spec("s2p3d1"), [(0, 2), (1, 3), (2, 1)])

    def test_basis_string_conventional(self):
        testing.assert_equal(parse_basis_spec_conventional("s2p3d1"), (2, 3, 1))
        testing.assert_equal(parse_basis_spec_conventional('d1'), (0, 0, 1))
        with self.assertRaises(ValueError):
            parse_basis_spec_conventional('s1p3s2')

    def test_nodes(self):
        with open(Path(__file__).parent / ".." / "test-data" / "Bi8.0.pao", 'r') as f:
            parser = PAOParser(f)
            radial_functions = parser.radial_functions()
        self.assertEqual(len(radial_functions), 5 * 15)
        for l in range(5):
            for n in range(15):
                r, f = radial_functions[l, n]
                zero_crosses = f[1:] * f[:-1] < 0
                self.assertEqual(sum(zero_crosses), n)
