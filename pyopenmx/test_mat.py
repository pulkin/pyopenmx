from unittest import TestCase
from pathlib import Path
import h5py
import numpy as np
from numericalunits import angstrom
from numpy import testing

from .mat import spline_paos, reciprocal_paos, index_order, overlap_pbc
from .parsers import PAOParser, parse_basis_spec_conventional


class TestUtil(TestCase):
    def test_index_order(self):
        order = index_order(np.array([0, 1, 0]), [
            parse_basis_spec_conventional('s1p2'),
            parse_basis_spec_conventional('p2d1'),
        ])
        # The index order of the above is:
        # #0 s:  0
        # #0 p:  3  1  2  6  4  5
        # #1 p:  9  7  8 12 10 11
        # #1 d: 15 17 13 16 14
        # #0 s: 18
        # #0 p: 21 19 20 24 22 23
        testing.assert_equal(order, [0, 3, 1, 2, 6, 4, 5, 9, 7, 8, 12, 10, 11, 15, 17, 13, 16, 14, 18, 21, 19, 20, 24,
                                     22, 23])


class BiTest(TestCase):
    @classmethod
    def setUpClass(cls, filename="openmx-hamiltonian.h5", basis='s1p1d1') -> None:

        cls.paos_k = {}
        with open(Path(__file__).parent / ".." / "test-data" / "Bi8.0.pao", 'r') as f:
            parser = PAOParser(f)
            paos = parser.radial_functions()
            paos_i = spline_paos(paos)
            cls.paos_k['bi'] = reciprocal_paos(paos_i)

        with h5py.File(Path(__file__).parent / ".." / "test-data" / filename, 'r') as f:
            cls.h_overlap = np.array(f["S"][..., 0])
            cls.h_ba = np.array(f["basis_atom"])
            cls.h_bo = np.array(f["basis_orbital"])
            cls.h_bs = np.array(f["basis_spin"])
            cls.h_vecs = np.array(f["vectors"])

        cls.c_vectors = np.array([[3, .2, .2], [.2, 3, .2], [.2, .2, 3]]) * angstrom
        cls.c_coords_cart = np.zeros((1, 3), dtype=float)
        cls.c_values = np.array(['bi'])
        cls.c_basis = {'bi': parse_basis_spec_conventional(basis)}

    def test_blocks(self):
        overlap = overlap_pbc(self.c_coords_cart, self.c_values, self.c_basis, self.paos_k,
                              translation_vectors=self.h_vecs @ self.c_vectors,
                              basis="openmx",
                              spin=True)
        testing.assert_allclose(overlap, self.h_overlap, atol=2e-4)


class BiTest2(BiTest):
    @classmethod
    def setUpClass(cls, filename="openmx-hamiltonian-s2p2d1.h5", basis='s2p2d1') -> None:
        super(BiTest2, cls).setUpClass(filename=filename, basis=basis)
