from unittest import TestCase
import numpy as np
from numpy import testing

from .util import ravel_orbital_index, orbital_order, c2s, radial2poly


class TestUtil(TestCase):
    def test_ravel(self):
        a = []
        for l in range(4):
            for m in orbital_order[l]:
                for n in range(3):
                    a.append(ravel_orbital_index(l, m, n, 3))
        testing.assert_equal(a, np.arange(len(a)))

    def test_c2s(self):
        testing.assert_allclose(c2s([0, 0, 0]), [0, np.pi/2, 0])
        testing.assert_allclose(c2s([[1, 0, 0], [0, 1, 0], [0, 0, 1]]),
                                [[1, np.pi/2, 0], [1, np.pi/2, np.pi/2], [1, 0, 0]])

    def test_r2p(self):
        x = np.arange(4)
        y = np.exp(-x ** 2 / 2)
        poly = radial2poly(x, y)
        testing.assert_allclose(poly(0), 1)
        testing.assert_allclose(poly(5), 0)
