from unittest import TestCase
import numpy as np
from numpy import testing

from .special import cg, gaunt, c2r_spherical_transform


class TestValues(TestCase):
    def test_clebsh_gordan(self):
        # https://en.wikipedia.org/wiki/Table_of_Clebsch%E2%80%93Gordan_coefficients
        cases = [
            # j1 m1 j2 m2 j  m
            ((0, 0, 0, 0, 0, 0), 1),
            ((0, 0, 0, 1, 0, 0), 0),
            ((.5, .5, 0, 0, .5, .5), 1),
            ((.5, -.5, 0, 0, .5, .5), 0),
            ((.5, .5, 0, 0, 0, 0), 0),
            ((.5, .5, .5, -.5, 1, 0), .5 ** .5),
            ((1.5, 1.5, .5, -.5, 1, 1), 3.**.5 / 2),
            ((1.5, -.5, 1, 1, 2.5, .5), .3 ** .5),
            ((2, 0, 1.5, .5, 3.5, .5), (18 / 35) ** .5),
            ((2, 0, 2, 0, 2, 0), - (2 / 7) ** .5),
            ((2.5, 2.5, 2, -1, 4.5, 1.5), 1 / 21 ** .5),
            ((1.5, 1.5, .5, -.5, 1, 1), 3.**.5 / 2),
        ]
        for x, r in cases:
            testing.assert_allclose(float(cg(*x)), r)

    def test_gaunt_0(self):
        testing.assert_allclose(float(gaunt(0, 0, 0, 0, 0, 0)), (4 * np.pi) ** -.5)

    def test_c2r_transform(self):
        testing.assert_allclose(c2r_spherical_transform(1), [
            [-1.j / 2.**.5, 0, -1.j / 2.**.5],
            [0, 1, 0],
            [1 / 2.**.5, 0, -1 / 2.**.5],
        ])
