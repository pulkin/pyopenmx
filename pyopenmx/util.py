import numpy as np
from scipy.interpolate import splrep, PPoly

# Order of OpenMX orbitals in the original implementation
# Values are quantum number m for real spherical harmonics
# See https://en.wikipedia.org/wiki/Table_of_spherical_harmonics#Real_spherical_harmonics
orbital_order = (
    (0,),
    (1, -1, 0),
    (0, 2, -2, 1, -1),
    (0, 1, -1, 2, -2, 3, -3),
)


def ravel_orbital_index(l, m, n, n_max):
    """
    Converts quantum numbers into a plain orbital index according to the
    original OpenMX implementation.

    Parameters
    ----------
    l : int
    m : int
    n : int
        Quantum numbers to convert.
    n_max : int
        Basis size per angular momentum.

    Returns
    -------
    result : int
        Orbital index.
    """
    if abs(m) > l:
        raise ValueError(f"|m = {m}| > l = {l}")
    if len(orbital_order) <= l:
        raise NotImplementedError("Higher-order orbital momentum not implemented")
    return n_max * (l ** 2 + orbital_order[l].index(m)) + n


def c2s(cartesian):
    """
    Converts cartesian to spherical coordinates.

    Parameters
    ----------
    cartesian : Iterable
        Cartesian coordinates to convert.

    Returns
    -------
    result : ndarray
        The resulting spherical coordinates (r, θ, φ).
    """
    cartesian = np.array(cartesian)
    result = np.empty_like(cartesian, dtype=float)
    result[..., 0] = radial = np.linalg.norm(cartesian, axis=-1)
    result[..., 1] = np.arccos(cartesian[..., 2] / (radial + np.finfo(float).tiny))
    result[..., 2] = np.arctan2(cartesian[..., 1], cartesian[..., 0])
    return result


def radial2poly(r, v, **kwargs):
    """
    Converts discretized data into a piecewise polynomial
    representing a radial function with a strict cut-off.

    Parameters
    ----------
    r : ndarray
        Grid points.
    v : ndarray
        Grid values.
    kwargs
        Keyword arguments to `scipy.interpolate.splrep`.

    Returns
    -------
    poly : PPoly
        A piecewise polynomial with the resulting function.
    """
    tck = splrep(r, v, **kwargs)
    order = tck[-1]
    poly = PPoly.from_spline(tck)
    poly.x = poly.x[order:-order + 1]
    poly.c = poly.c[:, order:-order + 1]
    poly.c[:, -1] = 0
    return poly
