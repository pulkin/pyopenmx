from dfttools.parsers.generic import AbstractTextParser
import numpy as np
from numericalunits import aBohr


def parse_basis_spec(spec):
    """
    Turns OpenMX basis string into (l, n) pairs.

    Parameters
    ----------
    spec : str
        OpenMX basis string: 's1', 'p1d2', etc.

    Returns
    -------
        A list of pairs (l, n).
    """
    spec = spec.lower()
    ln = []
    while len(spec) > 0:
        angular_symbol = spec[0]
        order = "spdfghiklmnop"
        angular_l = order.index(angular_symbol)
        spec = spec[1:]
        n = ""
        for i in spec:
            if i in "0123456789":
                n += i
            else:
                break

        spec = spec[len(n):]
        ln.append((angular_l, int(n)))
    return ln


def parse_basis_spec_conventional(spec):
    """
    Turns OpenMX basis string into a list of
    quantum numbers `n` per each `l`.

    Parameters
    ----------
    spec : str
        OpenMX basis string: 's1', 'p1d2', etc.

    Returns
    -------
        A list of quantum numbers `n`.
    """
    ln = np.array(parse_basis_spec(spec), dtype=int)
    if len(np.unique(ln[:, 0])) < len(ln):
        raise ValueError("One or more 'l' entries are not unique: the behavior is undefined in this case")
    result = np.zeros(ln[:, 0].max() + 1, dtype=int)
    result[ln[:, 0]] = ln[:, 1]
    return result


class PAOParser(AbstractTextParser):
    @staticmethod
    def valid_header(header):
        return "<Contraction.coefficients1" in header

    @staticmethod
    def valid_filename(name):
        return name.endswith(".pao")

    def radial_functions(self, normalization="3d", trim=True):
        """
        Parses radial functions.

        Parameters
        ----------
        normalization : {'1d', '3d'}
            Indicates normalization: 1D (radial part integrates to 1) or 3D (radial part
            integrates to 1/4π). 1D normalization is assumed for the PAO data.
        trim : bool
            If True, trims the zero tail of the orbital.

        Returns
        -------
        result : dict
            A dictionary where keys are tuples of quantum numbers (l, n) and values are pairs of
            arrays: radial points and function values at each point.
        """
        if normalization not in ('1d', '3d'):
            raise ValueError(f"normalization has to be either '1d' or '3d' (found: {normalization:r})")
        self.parser.reset()
        self.parser.skip("********  DATA for multiple pseudo atomic orbitals  *******")
        self.parser.skip("PAO.Lmax")
        lmax = self.parser.next_int()
        self.parser.skip("PAO.Mul")
        mul = self.parser.next_int()

        result = {}

        for l in range(lmax + 1):
            self.parser.skip("<pseudo.atomic.orbitals.L=" + str(l))
            data = self.parser.next_float(n="pseudo.atomic.orbitals.L=" + str(l)).reshape((-1, mul + 2))
            rl = data[:, 1]
            funcs = data[:, 2:]
            if normalization == '3d':
                funcs /= (4 * np.pi) ** .5
            for n in range(funcs.shape[1]):
                f = funcs[:, n]
                if trim:
                    f = np.trim_zeros(f, trim='b')
                result[l, n] = (rl[:len(f)] * aBohr, f / aBohr ** 1.5)

        return result
