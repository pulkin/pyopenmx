import numpy as np
from scipy.linalg import block_diag

from .util import radial2poly, c2s, orbital_order
from .quad import rft_cst, q2c_ovlp_radial_quad, q2c_ovlp
from .special import c2r_spherical_transform


def spline_paos(paos, **kwargs):
    """
    Interpolate projected atomic orbitals (PAOs) by splines.

    Parameters
    ----------
    paos : dict
        A dictionary with keys being (l, n) quantum numbers and values being two arrays:
        PAO radial coordinates and PAO values.
    kwargs
        Keyword arguments to `scipy.interpolate.splrep`.

    Returns
    -------
    paos : dict
        A dictionary where discretized PAOs were replaced by splines.
    """
    return {
        k: radial2poly(*v, **kwargs)
        for k, v in paos.items()
    }


def reciprocal_paos(paos, cutoff_x=50, np=64, **kwargs):
    """
    Transform real-space projected atomic orbitals (PAOs) into the reciprocal space.

    Parameters
    ----------
    paos : dict
        A dictionary with keys being (l, n) quantum numbers and values being PAOs (PPoly).
    cutoff_x : float
        Cutoff multiplier.
    np : int
        The number of points within cutoff.
    kwargs
        Keyword arguments to `rft_cst`.

    Returns
    -------
    paos : dict
        A dictionary where real-space PAOs were replaced by reciprocal-space ones.
    """
    return {
        (l, n): rft_cst(v, l, v.x.max() * cutoff_x, int(np * cutoff_x), **kwargs)
        for (l, n), v in paos.items()
    }


def iter_complex_basis(atoms, basis_sizes):
    """
    Iterates over the basis of complex spherical harmonics.

    Parameters
    ----------
    atoms : ndarray
        An array with keys to `bases`.
    basis_sizes : list, dict
        Lookup tables with `n` quantum numbers per each `l`,
        for each atom key.

    Yields
    ------
    atom_id : int, atom, l : int, m : int, n : int
        Index in `atoms` array, the corresponding value and 3 quantum numbers (l, m, n).
    """
    for atom_id, atom in enumerate(atoms):
        for l, n_max in enumerate(basis_sizes[atom]):
            for n in range(n_max):
                for m in range(-l, l+1):
                    yield atom_id, atom, l, m, n


def overlap_complex(coordinates_row, atoms_row, coordinates_col, atoms_col, basis_sizes, rrf):
    """
    Prepares an overlap matrix in the basis of complex
    spherical harmonics.

    Parameters
    ----------
    coordinates_row : ndarray
    atoms_row : ndarray
        Coordinates and keys of atoms corresponding to the
        first index of the resulting matrix.
    coordinates_col : ndarray
    atoms_col : ndarray
        Coordinates and keys of atoms corresponding to the
        second index of the resulting matrix.
    basis_sizes : list, dict
        Lookup tables with `n` quantum numbers per each `l`,
        for each atom key.
    rrf : dict
        A dict with keys being (l, n) and values being
        reciprocal radial functions.

    Returns
    -------
    result : ndarray
        The resulting overlap matrix.
    """
    row_basis = tuple(iter_complex_basis(atoms_row, basis_sizes))
    col_basis = tuple(iter_complex_basis(atoms_col, basis_sizes))

    result = np.empty((len(row_basis), len(col_basis)), dtype=np.complex)

    for row, (atom_id_row, atom_row, l_row, m_row, n_row) in enumerate(row_basis):
        radial_row = rrf[atom_row][l_row, n_row]
        xyz_row = coordinates_row[atom_id_row]
        for col, (atom_id_col, atom_col, l_col, m_col, n_col) in enumerate(col_basis):
            radial_col = rrf[atom_row][l_col, n_col]
            xyz_col = coordinates_col[atom_id_col]
            tau, theta, phi = c2s(xyz_col - xyz_row)
            common_l = max(l_col, l_row)

            # TODO: implement caching here
            radial_quads = list(
                q2c_ovlp_radial_quad(radial_row, radial_col, tau, _l)
                for _l in range(2 * common_l + 1)
            )

            result[row, col] = q2c_ovlp(l_row, m_row, l_col, m_col, theta, phi, radial_quads)

    return result


def c2r_transform(atoms, bases):
    """
    Prepares a matrix to transform from the complex to real
    basis.

    Parameters
    ----------
    atoms : ndarray
        Keys of atoms.
    bases : list, dict
        Lookup tables with `n` quantum numbers per each `l`,
        for each atom key.

    Returns
    -------
    result : ndarray
        The transform matrix.
    """
    blocks = []
    for atom in atoms:
        for l, max_n in enumerate(bases[atom]):
            if max_n > 0:
                m = c2r_spherical_transform(l)
                blocks += [m] * max_n
    return block_diag(*blocks)


def index_order(atoms, basis_sizes):
    """
    OpenMX index ordering of real spherical Harmonics.

    Parameters
    ----------
    atoms : ndarray
        Keys of atoms.
    basis_sizes : list, dict
        Lookup tables with `n` quantum numbers per each `l`,
        for each atom key.

    Returns
    -------
    result : ndarray
        Indices of OpenMX orbitals.
    """
    result = []
    global_offset = 0
    for atom_id, atom in enumerate(atoms):
        for l, n_max in enumerate(basis_sizes[atom]):
            order = np.array(orbital_order[l])  # runs from -l to l
            order += l  # runs from 0 to 2l
            nl = len(order)
            offsets = np.arange(0, n_max) * nl
            order = order[np.newaxis, :] + offsets[:, np.newaxis]
            order = order.reshape(-1)
            order += global_offset
            global_offset += len(order)
            result.append(order)
    return np.concatenate(result)


def overlap_pbc(coordinates, atoms, basis_sizes, rrf, translation_vectors=None, basis="openmx", spin=False):
    """
    Prepares an overlap matrix in the OpenMX basis.

    Parameters
    ----------
    coordinates : ndarray
    atoms : ndarray
        Coordinates and keys of atoms in theunit cell.
    basis_sizes : list, dict
        Lookup tables with `n` quantum numbers per each `l`,
        for each atom key.
    rrf : dict
        A dict with keys being (l, n) and values being
        reciprocal radial functions.
    translation_vectors : ndarray
        A 2D array of crystal [translation] vectors specified
        in cartesian coordinates.
    basis : {'complex', 'real', 'openmx'}
        Spherical harmonics choice for the output basis.
    spin : bool
        If True, adds the spin dimension by putting two identical
        blocks of the overlap matrix onto a diagonal.

    Returns
    -------
    result : ndarray
        The resulting overlap matrix as a 3D array where the
        first dimension corresponds to `translation_vectors`
        specified.
    """
    if translation_vectors is None:
        translation_vectors = np.zeros(3)
    if translation_vectors.ndim == 1:
        translation_vectors = translation_vectors[np.newaxis, :]
        squeeze = True
    else:
        squeeze = False

    if basis == 'complex':
        c2r = reorder = False
    elif basis == 'real':
        c2r = True
        reorder = False
    elif basis == 'openmx':
        c2r = reorder = True
    else:
        raise ValueError(f"Unknown basis: {repr(basis)}")

    result = []
    for vector in translation_vectors:
        result.append(overlap_complex(coordinates, atoms, coordinates + vector[np.newaxis, :], atoms, basis_sizes, rrf))

    result = np.array(result)
    if c2r:
        transform_rows = c2r_transform(atoms, basis_sizes)
        transform_cols = transform_rows.conj().T
        result = np.einsum("ia,xab,bj->xij", transform_rows, result, transform_cols, optimize=True).real  # cast 2 real

    if reorder:
        order = index_order(atoms, basis_sizes)
        result = result[:, order, :][:, :, order]

    if spin:
        x, b = result.shape[:2]
        result_spin = np.zeros((x, 2 * b, 2 * b), dtype=result.dtype)
        result_spin[:, :b, :b] = result
        result_spin[:, b:, b:] = result
        result = result_spin

    if squeeze:
        result = result[0]

    return result
