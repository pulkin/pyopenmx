import numpy as np
from scipy import integrate, special, fft, interpolate

from .special import gaunt
from .util import radial2poly


def cmplx_quad(f, *args, **kwargs):
    """
    Wraps `scipy.integrate` for complex-valued output.

    Parameters
    ----------
    f : Callable
        The integrand.
    args
    kwargs
        Arguments to `integrate.quad`.

    Returns
    -------
        Complex-valued result of the integration.
    """
    def f_r(x):
        return f(x).real

    def f_i(x):
        return f(x).imag

    real_part = integrate.quad(f_r, *args, **kwargs)[0]
    imag_part = integrate.quad(f_i, *args, **kwargs)[0]
    return real_part + 1j*imag_part


def rft_prefactor(l, inverse):
    """
    Prefactor for radial Fourier transforms.

    Value: 1/sqrt(2π)^3 · 4π · (-i)^l

    Parameters
    ----------
    l : int
        Angular momentum quantum number.
    inverse : bool
        If True, returns a prefactor for inverse transform.

    Returns
    -------
    result : float
        Prefactor value.
    """
    result = 1 / (2 * np.pi) ** 1.5 * 4 * np.pi * (-1.j) ** l
    if inverse:
        result *= (-1) ** l
    return result


def grids(cutoff, n, shift_r=False, shift_k=False):
    """
    Prepares real and reciprocal grids for the given cutoff.

    Parameters
    ----------
    cutoff : float
        Real-space cutoff.
    n : int
        The number of grid points.
    shift_r : bool, float
    shift_k : bool, float
        If set, shifts the corresponding grid (real or reciprocal)
        by the specified fraction of spacing.

    Returns
    -------
    result : ndarray, ndarray
        Real-space and reciprocal-space grids.
    """
    # Type-II discrete cosine transform
    # y(k) = ... cos(pi * k * (2n+1) / 2 / N)
    # The maximal value of cos argument is ~ pi N
    # Thus, cutoff * cutoff_k = N pi
    dr = cutoff / n
    dk = np.pi / cutoff
    r = (np.arange(n) + shift_r) * dr
    k = (np.arange(n) + shift_k) * dk
    return r, k


def _rft_bessel(f, l, cutoff, inverse=False, quad_kwargs=None):
    """
    Radial Fourier transform of a black-box function through an integral with Bessel
    functions.

    Definition: f(k) = 1/sqrt(2π)^3 · 4π · (-i)^l · int(r^2·f(r)·j_l(kr))

    Source: Eq. 42 of http://www.openmx-square.org/tech_notes/tech1-1_2/tech1-1_2.html

    Parameters
    ----------
    f : Callable
        Radial function.
    l : int
        Angular momentum.
    cutoff : float
        Radial function cutoff.
    inverse : bool
        If True, performs inverse transformation.
    quad_kwargs : dict
        Keyword arguments to `scipy.integrate.quad` performing
        Bessel transform.

    Returns
    -------
    result : Callable
        A reciprocal radial function.
    """
    if quad_kwargs is None:
        quad_kwargs = dict()

    prefactor = rft_prefactor(l, inverse)

    def _result(k):
        return prefactor * cmplx_quad(
            lambda x: x ** 2 * f(x) * special.spherical_jn(l, k * x), 0, cutoff,
            points=np.arange(0, cutoff, 2 * np.pi / k) if k != 0 else None,
            **quad_kwargs
        )

    return _result


def rft_bessel(f, l, cutoff, n, inverse=False, dense=4, quad_kwargs=None, spline_kwargs=None):
    """
    Radial Fourier transform of a black-box function through an integral with Bessel
    functions. Calculates the integral returned by `_rft_bessel` on a grid and wraps
    it into a piecewise polynomial for faster evaluation.

    Definition: f(k) = 1/sqrt(2π)^3 · 4π · (-i)^l · int(r^2·f(r)·j_l(kr))

    Source: Eq. 42 of http://www.openmx-square.org/tech_notes/tech1-1_2/tech1-1_2.html

    Parameters
    ----------
    f : Callable
        A function to transform.
    l : int
        Angular momentum.
    cutoff : float
        Radial function cutoff.
    n : int
        The number of discretization points.
    inverse : bool
        If True, performs inverse transformation.
    dense : int
        Dense k-point grid factor.
    quad_kwargs : dict
        Keyword arguments to `scipy.integrate.quad` performing
        sine/cosine transform.
    spline_kwargs : dict
        Keyword arguments to `scipy.interpolate.splrep` interpolating
        sine/cosine-transformed function.

    Returns
    -------
    result : Callable
        The resulting function interpolator.
    """
    if spline_kwargs is None:
        spline_kwargs = dict()
    _, k_grid_dense = grids(cutoff * dense, n * dense)
    result = _rft_bessel(f, l, cutoff, inverse=inverse, quad_kwargs=quad_kwargs)
    vals = np.array(tuple(map(result, k_grid_dense)))
    # Convert to real
    if np.all(vals.real == 0):
        prefactor = 1.j
        vals = vals.imag
    elif np.all(vals.imag == 0):
        prefactor = 1
        vals = vals.real
    else:
        raise NotImplementedError("Cannot prepare a spline for a general complex-valued function")
    poly = radial2poly(k_grid_dense, vals, **spline_kwargs)
    poly.c = poly.c * prefactor
    return poly


def rft_cst(f, l, cutoff, n, inverse=False, dense=1, quad_kwargs=None, spline_kwargs=None):
    """
    Radial Fourier transform of a black-box function through 1D cosine/sine
    transforms.

    Implements several equations from http://openmx-square.org/exx/CPC2009/

    Parameters
    ----------
    f : Callable
        A function to transform.
    l : int
        Angular momentum.
    cutoff : float
        Radial function cutoff.
    n : int
        The number of discretization points.
    inverse : bool
        If True, performs inverse transformation.
    dense : int
        Dense k-point grid factor.
    quad_kwargs : dict
        Keyword arguments to `scipy.integrate.quad` performing
        sine/cosine transform.
    spline_kwargs : dict
        Keyword arguments to `scipy.interpolate.splrep` interpolating
        sine/cosine-transformed function.

    Returns
    -------
    result : Callable
        The resulting function interpolator.
    """
    if spline_kwargs is None:
        spline_kwargs = dict()
    if quad_kwargs is None:
        quad_kwargs = dict()

    # 1: Prepare grids
    # - for sine/cosine transform
    # - a dense grid for final interpolation
    r_grid, k_grid = grids(cutoff, n, shift_r=.5, shift_k=l % 2)
    _, k_grid_dense = grids(cutoff * dense, n * dense)

    # 2: Compute sine/cosine transform
    # for the function and its derivatives
    # F_n(t) = int dr cos(tr) · f(r) · r^2
    # Eq. 8, 19, 20
    if l % 2 == 0:
        transform_F = fft.dct(f(r_grid) * r_grid ** 2)
    else:
        transform_F = fft.dst(f(r_grid) * r_grid ** 2)

    transform_F *= (r_grid[1] - r_grid[0]) / 2

    # 3: Make a smooth function (polynomial) out of transformed data
    if l % 2 == 0:
        poly = radial2poly(k_grid, transform_F, **spline_kwargs)
    else:
        # for sine transform add a zero-zero point
        poly = radial2poly(
            np.concatenate(([0], k_grid)),
            np.concatenate(([0], transform_F)),
            **spline_kwargs
        )
    k_grid_spline = poly.x

    # Multiply the polynomial t * F_n(t) if n is odd
    if l % 2 == 1:
        c1 = np.pad(poly.c, ((0, 1), (0, 0)), 'constant')
        c2 = np.pad(poly.c, ((1, 0), (0, 0)), 'constant')
        poly.c = c2 * k_grid_spline[np.newaxis, :-1] + c1

    # 4: evaluate integrals Eq. 11 on a dense grid
    # l=6 -> j=0..3 -> n_list=0, 2, 4, 6
    # l=7 -> j=0..3 -> n_list=1, 3, 5, 7
    j_list = np.arange(l // 2 + 1)
    n_list = np.arange(l % 2, l + 1, 2)
    I_poly = []

    for n in n_list:

        # Integrate
        poly_int = poly.antiderivative()

        # Calculate at grid points
        poly_vals = poly_int(k_grid_dense)

        # Outside origin: divide right away
        mask = k_grid_dense != 0
        poly_vals[mask] /= k_grid_dense[mask] ** (n + 1)

        # At the origin: regularize
        mask = np.logical_not(mask)
        if np.any(mask):
            # Make sure it does not diverge
            for i in range(n+1):
                assert poly_int.derivative(nu=i)(0) < 1e-12
            poly_vals[mask] = poly_int.derivative(n + 1)(0) / special.factorial(n + 1)

        I_poly.append(poly_vals)

        # Preapre for the next round by multiplying by t^2 for the next t^n * F_n(t)
        # TODO: remove it for the last iteration
        c1 = np.pad(poly.c, ((0, 2), (0, 0)), 'constant')
        c2 = np.pad(poly.c, ((1, 1), (0, 0)), 'constant')
        c3 = np.pad(poly.c, ((2, 0), (0, 0)), 'constant')
        x = k_grid_spline[np.newaxis, :-1]
        poly.c = c3 * x ** 2 + c2 * 2 * x + c1

    # 5: Prepare prefactors
    n = j_list[-1]
    if l % 2 == 0:
        prefactor = (-1.0) ** j_list * special.factorial2(2 * n + 2 * j_list - 1) / special.factorial2(2 * n - 2 * j_list) / special.factorial(
            2 * j_list)
    else:
        prefactor = (-1.0) ** j_list * special.factorial2(2 * n + 2 * j_list + 1) / special.factorial2(2 * n - 2 * j_list) / special.factorial(
            2 * j_list + 1)

    # 6: Sum up everything and transform into a polynomial on a dense grid
    final = (prefactor[:, np.newaxis] * np.array(I_poly)).sum(axis=0)
    final_poly = radial2poly(k_grid_dense, final, **spline_kwargs)
    final_poly.c = final_poly.c * rft_prefactor(l, inverse)
    return final_poly


def q2c_ovlp_radial_quad(f1, f2, tau, l, cutoff=None, driver='trapz', **kwargs):
    """
    The radial quadrature part of the two-center overlap integral:
    analytic integration.

    Definition: int(k^2 · j_l(k·tau) · f1(k)* · f2(k))

    Source: Eq. 43 of http://www.openmx-square.org/tech_notes/tech1-1_2/tech1-1_2.html

    Parameters
    ----------
    f1 : Callable
    f2 : Callable
        Radial components of the reciprocal spherical function.
    tau : float
        Distance between centers of spherical functions.
    l : int
        Spherical function order.
    cutoff : float
        Common cutoff of spherical functions.
    driver : {'quad', 'trapz'}
        Quadrature driver: `scipy.integrate.quad` (for generic callable inputs)
        or `numpy.trapz` (for PPoly inputs only).
    kwargs
        Arguments to `cmplx_quad`.

    Returns
    -------
    result : complex
        The value of the quadrature.
    """
    def _integrand(k):
        return k ** 2 * f1(k).conjugate() * f2(k) * special.spherical_jn(l, k * tau)

    poly_input = isinstance(f1, interpolate.PPoly) and isinstance(f2, interpolate.PPoly)

    if cutoff is None:
        if poly_input:
            cutoff = max(f1.x.max(), f2.x.max())
        else:
            raise ValueError("Cutoff argument required for non-PPoly inputs")

    if driver == 'quad':
        return cmplx_quad(_integrand, 0, cutoff, **kwargs)

    elif driver == 'trapz':
        if not poly_input:
            raise ValueError("'trapz' driver can only be used in conjunction with PPoly inputs")
        x = f1.x
        return np.trapz(_integrand(x), x=x)

    else:
        raise ValueError(f'driver argument must be one of "quad", "poly"; found: {repr(driver)}')


def q2c_ovlp(l1, m1, l2, m2, theta, phi, radial_quad):
    """
    Two-center overlap quadrature.

    Definition: (4π)^2 sum_{l, m = -l..l} ([-i]^l · Y_{lm}(theta, phi) · Gaunt(l, -m, l1, m1, l2, m2))

    Source: Eq. 43 of http://www.openmx-square.org/tech_notes/tech1-1_2/tech1-1_2.html
    Additional factor of 4 pi is missing there: it is included into the numerical radial part instead.

    Parameters
    ----------
    l1 : int
    m1 : int
    l2 : int
    m2 : int
        Spherical harmonics specs.
    theta : float
    phi : float
        Angular coordinates of the displacement vector.
    radial_quad : Iterable
        Radial l-dependent quadratures.

    Returns
    -------
    result : complex
        The value of the integral.
    """
    result = 0
    for l, _radial_quad in enumerate(radial_quad):
        for m in range(-l, l + 1):
            # TODO: figure out whether -m1 is needed (probably not)
            prefactor = (4 * np.pi) ** 2 * ((-1j) ** l) * special.sph_harm(m, l, phi, theta).conj() * gaunt(l1, m1, l2, m2, l, m)
            result += prefactor * _radial_quad
    return result
