import numpy as np
from numpy import pi
from math import factorial

def _f(x):
    return factorial(int(round(x)))


def _cg(j1, m1, j2, m2, j, m):
    if m != m1 + m2:
        return 0

    k_min = int(max(0, -(j - j2 + m1), -(j - j1 - m2)))
    k_max = int(min(j1 + j2 - j, j1 - m1, j2 + m2))
    if k_min > k_max:
        return 0

    s = 0
    for k in range(k_min, k_max + 1):
        s += (-1) ** k / (_f(k) * _f(int(j1 + j2 - j) - k) * _f(j1 - m1 - k) * _f(j2 + m2 - k) * _f(j - j2 + m1 + k) *
                          _f(j - j1 - m2 + k))

    return ((2 * j + 1) * _f(j + j1 - j2) * _f(j - j1 + j2) * _f(j1 + j2 - j) / _f(j1 + j2 + j + 1)) ** .5 * \
           (_f(j + m) * _f(j - m) * _f(j1 - m1) * _f(j1 + m1) * _f(j2 - m2) * _f(j2 + m2)) ** .5 * s


def cg(j1, m1, j2, m2, j, m):
    """
    Clebsch-Gordan coefficients.

    Definition: <j1 j2; m1 m2 | j1 j2; J M> = δ(m,m1+m2) ·
     sqrt([2J+1]·[J+j1-j2]!·[J-j1+j2]!·[j1+j2-J]! / [j1+j2+J+1]!) ·
     sqrt([J+M]!·[J-M]!·[j1-m1]!·[j1+m1]!·[j2-m2]!·[j2+m2]!) ·
     sum_k {
        (-1)^k / (k!·[j1+j2-J-k]!·[j1-m1-k]!·[j2+m2-k]!·[J-j2+m1+k]!·[J-j1-m2+k]!)
     }

    Source: https://en.wikipedia.org/wiki/Clebsch%E2%80%93Gordan_coefficients

    Parameters
    ----------
    j1
    m1
    j2
    m2
    j
    m
        Half-integer inputs.

    Returns
    -------
    result : float
        The coefficient value.
    """
    for k, v in locals().items():
        if v % 0.5 != 0:
            raise ValueError(f"Half-integer for '{k}' expected, found: {v}")
    if j1 % 1 != m1 % 1:
        raise ValueError(f"Inconsistent j1 % 1 = {j1 % 1} != m1 % 1 = {m1 % 1}")
    if j2 % 1 != m2 % 1:
        raise ValueError(f"Inconsistent j2 % 1 = {j2 % 1} != m2 % 1 = {m2 % 1}")
    if j % 1 != m % 1:
        raise ValueError(f"Inconsistent j % 1 = {j % 1} != m % 1 = {m % 1}")
    return _cg(j1, m1, j2, m2, j, m)


def gaunt(l, m, l1, m1, l2, m2):
    """
    Gaunt coefficients.

    Definition: int(Y_l^m* · Y_l1^m1 · Y_l2^m2) = sqrt([2*l1+1]·[2*l2+1]/4/π) ·
     <l1 l2; 0 0 | l1 l2; l 0> · <l1 l2; m1 m2 | l1 l2; l m>

    Source: Sakurai Eq. 3.7.73 (p. 217)

    Parameters
    ----------
    l
    m
    l1
    m1
    l2
    m2
        Half-integer inputs.

    Returns
    -------
    result : float
        THe Gaunt coefficient.
    """
    return ((2 * l1 + 1) * (2 * l2 + 1) / (2 * l + 1) / 4 / pi) ** .5 * cg(l1, 0, l2, 0, l, 0) * cg(l1, m1, l2, m2, l, m)


def c2r_spherical_transform(l):
    """
    Complex to real spherical harmonics transformation matrix.

    Definition of real harmonics:
        Y'_lm = i/sqrt(2) · [Y_lm - (-1)^m Y_{l,-m}], m < 0
        Y'_lm = Y_lm, m = 0
        Y'_lm = 1/sqrt(2) · [Y_{l,-m} + (-1)^m Y_lm], m > 0

    Source: https://en.wikipedia.org/wiki/Spherical_harmonics

    The indices of rows and columns are sums of quantum numbers m and l.

    Parameters
    ----------
    l : int
        Angular momentum quantum number.

    Returns
    -------
    result : ndarray
        The transformation matrix.
    """
    result = np.zeros((2 * l + 1, 2 * l + 1), dtype=np.complex)

    for m in range(1, l + 1):
        result[l - m, l - m] = -1j
        result[l - m, l + m] = 1.j * (-1) ** m
        result[l + m, l - m] = 1
        result[l + m, l + m] = (-1) ** m
    result /= 2 ** .5
    result[l, l] = 1
    return result
