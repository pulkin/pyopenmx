from unittest import TestCase
import numpy as np
from numpy import testing, exp
from math import pi
from pathlib import Path
import scipy.linalg as la
from scipy.special import factorial2
from numericalunits import angstrom
import h5py
from itertools import product

from .quad import cmplx_quad, grids, rft_bessel, rft_cst, q2c_ovlp_radial_quad, q2c_ovlp
from .parsers import PAOParser
from .util import ravel_orbital_index, c2s
from .special import c2r_spherical_transform
from .mat import spline_paos, reciprocal_paos, index_order


def normalized_gaussian(l, a=None):
    """
    Prepares a normalized gaussian in 3D.

    Parameters
    ----------
    l : int
        Angular momentum.
    a : complex
        Optional prefactor.

    Returns
    -------
    result : Callable
        A callable f(r) returning the radial part of the gaussian.
    """
    chi = .5
    prefactor = 1 / (2 * np.pi * chi) ** .25 * (
        (4 * chi) ** (l + 2) / factorial2(2 * l + 1)
    ) ** .5 / 2 / np.pi ** .5

    if a is not None:
        prefactor *= a

    def _result(x):
        return np.exp(-chi * x ** 2) * prefactor * x ** l  # / pi ** (3 / 4)

    return _result


class TestUtil(TestCase):
    def test_cmplx_quad(self):
        """Test complex-valued integrals on a complex arc"""
        def f(x):
            return exp(1.j * x)

        testing.assert_allclose(cmplx_quad(f, 0, 2 * pi), 0, atol=1e-14)
        testing.assert_allclose(cmplx_quad(f, 0, pi), 2j, atol=1e-14)

    def test_grids(self):
        """Tests grids formation"""
        cutoff_r = np.pi
        n = 3
        r, k = grids(cutoff_r, n,)
        testing.assert_allclose(r, np.arange(n) / n * cutoff_r)
        testing.assert_allclose(k, np.arange(n))

        r, k = grids(cutoff_r, n, .5, 0)
        testing.assert_allclose(r, np.arange(.5, n) / n * cutoff_r)
        testing.assert_allclose(k, np.arange(n))

        r, k = grids(cutoff_r, n, 0, .5)
        testing.assert_allclose(r, np.arange(n) / n * cutoff_r)
        testing.assert_allclose(k, np.arange(.5, n))


class TestGaussians(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        def g(x):
            return exp(-x ** 2)

        def gt(x):
            return exp(-x ** 2 / 4) / 2. ** 1.5

        cls.g = staticmethod(g)
        cls.g_t = staticmethod(gt)

        cls.gn = list(normalized_gaussian(l) for l in range(4))
        # TODO: figure out whether -j^l prefactor is correct
        cls.gn_t = list(normalized_gaussian(l, a=(-1.j) ** l) for l in range(4))

        cls.k_grid = np.linspace(0, 5)

    def __test_ft_callable__(self, f, n, cutoff, inverse=False, dense=4, **kwargs):
        import warnings
        warnings.filterwarnings(action="error", category=np.ComplexWarning)

        # Simple Gaussian test
        g_t = f(self.g, 0, cutoff, n, inverse=inverse, dense=dense)
        y = g_t(self.k_grid)
        y_ref = self.g_t(self.k_grid)
        testing.assert_allclose(y, y_ref, **kwargs)

        # Normalized Gaussians
        for l, (g, g_t_ref) in enumerate(zip(self.gn, self.gn_t)):
            g_t = f(g, l, cutoff, n, inverse=inverse, dense=dense)
            y = g_t(self.k_grid)
            y_ref = g_t_ref(self.k_grid)
            if inverse and l % 2 == 1:
                y_ref = - y_ref
            testing.assert_allclose(y, y_ref, err_msg=f"l={l:d}", **kwargs)

    def test_rft_bessel(self):
        """Test direct Bessel quadrature in radial Fourier transform"""
        self.__test_ft_callable__(rft_bessel, n=30, cutoff=10, atol=1e-8, dense=16)
        self.__test_ft_callable__(rft_bessel, n=30, cutoff=10, atol=1e-8, inverse=True, dense=16)

    def test_rft_cst(self):
        """Test cosine/sine quadrature in radial Fourier transform"""
        # A large cutoff is needed here to suppress PBC in discrete transforms
        self.__test_ft_callable__(rft_cst, n=450, cutoff=150, atol=1e-8)
        self.__test_ft_callable__(rft_cst, n=450, cutoff=150, atol=1e-8, inverse=True)

    def test_gaussian_norm_s(self):
        """Test normalization of a simple gaussian"""
        gaussian = normalized_gaussian(0)
        qrad = q2c_ovlp_radial_quad(gaussian, gaussian, 0, 0, 10, driver='quad')
        testing.assert_allclose(qrad, .25 / np.pi)
        q = q2c_ovlp(0, 0, 0, 0, 0, 0, [qrad])
        testing.assert_allclose(q, 1)

    def test_gaussian_orthogonality(self):
        """Test orthogonality of spherical functions with a gaussisn"""
        gaussian = normalized_gaussian(0)
        qrad = list(q2c_ovlp_radial_quad(gaussian, gaussian, 0, l, 10, driver='quad') for l in range(4))
        for l1 in range(4):
            for m1 in range(-l1, l1+1):
                for l2 in range(4):
                    for m2 in range(-l2, l2+1):
                        testing.assert_allclose(q2c_ovlp(l1, m1, l2, m2, 0, 0, qrad), l1 == l2 and m1 == m2)

    def test_gaussian_ovlp_s(self):
        """Test overlap of a pair of gaussians shifted by a vector"""
        tau = 1.52
        theta = 1
        phi = 2
        result = np.exp(-tau ** 2 / 4)
        gaussian = normalized_gaussian(0)
        qrad = q2c_ovlp_radial_quad(gaussian, gaussian, tau, 0, 10, driver='quad')
        testing.assert_allclose(qrad, .25 / np.pi * result)
        q = q2c_ovlp(0, 0, 0, 0, theta, phi, [qrad])
        testing.assert_allclose(q, result)


class TestBi(TestCase):
    @classmethod
    def setUpClass(cls) -> None:

        with open(Path(__file__).parent / ".." / "test-data" / "Bi8.0.pao", 'r') as f:
            parser = PAOParser(f)
            cls.paos = parser.radial_functions()
            cls.paos_i = spline_paos(cls.paos)
            cls.paos_k = reciprocal_paos(cls.paos_i)

        with h5py.File(Path(__file__).parent / ".." / "test-data" / "openmx-hamiltonian.h5", 'r') as f:
            cls.h_overlap = np.array(f["S"][..., 0])
            cls.h_ba = np.array(f["basis_atom"])
            cls.h_bo = np.array(f["basis_orbital"])
            cls.h_bs = np.array(f["basis_spin"])
            cls.h_vecs = np.array(f["vectors"])

        cls.c_vectors = np.array([[3, .2, .2], [.2, 3, .2], [.2, .2, 3]]) * angstrom
        cls.shift_vector = np.array([1, 0, 0])
        cls.shift_vector_id = np.argwhere(np.all(cls.h_vecs == cls.shift_vector[np.newaxis, :], axis=1)).squeeze().item()

    def test_norm(self):
        """Tests normalization of PAOs"""
        for (l, n), f in self.paos_k.items():
            qrad = list(q2c_ovlp_radial_quad(f, f, 0, _l) for _l in range(2 * l + 1))
            for m in range(-l, l+1):
                testing.assert_allclose(
                    q2c_ovlp(l, m, l, m, 0, 0, qrad), 1,
                    err_msg=f'l={l} m={m} n={n}', atol=2e-4)

    def __calc_overlap__(self, orb1, orb2):
        tau, theta, phi = c2s(self.shift_vector @ self.c_vectors)

        v_reciprocal = []
        for orb in orb1, orb2:
            orb_l, orb_m, orb_n = orb
            v_reciprocal.append(self.paos_k[orb_l, orb_n])

        v_radial_quad = list(
            q2c_ovlp_radial_quad(*v_reciprocal, tau, _l)
            for _l in range(2 * max(orb1[0], orb2[0]) + 1)
        )

        return q2c_ovlp(*orb1[:2], *orb2[:2], theta, phi, v_radial_quad)

    def test_overlap_ss(self):
        orb1 = 0, 0, 0
        orb2 = 0, 0, 0

        orb_ix = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb1, orb2))

        testing.assert_allclose(
            self.__calc_overlap__(orb1, orb2),
            self.h_overlap[self.shift_vector_id, orb_ix[0], orb_ix[1]],
            atol=1e-5,
            rtol=1e-3,
        )

    def test_overlap_spz(self):
        orb1 = 0, 0, 0
        orb2 = 1, 0, 0

        orb_ix = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb1, orb2))

        testing.assert_allclose(
            self.__calc_overlap__(orb1, orb2),
            self.h_overlap[self.shift_vector_id, orb_ix[0], orb_ix[1]],
            atol=1e-5,
            rtol=1e-3,
        )

    def test_overlap_spxy(self):
        orb1 = 0, 0, 0
        orb2m = 1, -1, 0
        orb2p = 1, 1, 0

        orb_ix1 = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb1, orb2m))
        orb_ix2 = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb1, orb2p))

        s1 = self.h_overlap[self.shift_vector_id, orb_ix1[0], orb_ix1[1]]
        s2 = self.h_overlap[self.shift_vector_id, orb_ix2[0], orb_ix2[1]]

        # Transform real to complex
        # real_minus = i/sqrt(2) (complex_minus + complex_plus)
        # real_plus = 1/sqrt(2) (complex_minus - complex_plus)
        # (i real_minus + real_plus) = - sqrt(2) * complex_plus
        # (i real_minus - real_plus) = - sqrt(2) * complex_minus
        sm = (s2 - 1j * s1) / 2.**.5
        sp = (-1j * s1 - s2) / 2.**.5

        testing.assert_allclose(self.__calc_overlap__(orb1, orb2m), sm, atol=1e-4, rtol=1e-3)
        testing.assert_allclose(self.__calc_overlap__(orb1, orb2p), sp, atol=1e-4, rtol=1e-3)

    def test_overlap_sdz2(self):
        orb1 = 0, 0, 0
        orb2 = 2, 0, 0

        orb_ix = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb1, orb2))

        testing.assert_allclose(
            self.__calc_overlap__(orb1, orb2),
            self.h_overlap[self.shift_vector_id, orb_ix[0], orb_ix[1]],
            atol=1e-5, rtol=1e-3,
        )

    def test_overlap_sdxzdyz(self):
        orb1 = 0, 0, 0
        orb2m = 2, -1, 0
        orb2p = 2, 1, 0

        orb_ix1 = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb1, orb2m))
        orb_ix2 = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb1, orb2p))

        s1 = self.h_overlap[self.shift_vector_id, orb_ix1[0], orb_ix1[1]]
        s2 = self.h_overlap[self.shift_vector_id, orb_ix2[0], orb_ix2[1]]

        # Transform real to complex
        # real_minus = i/sqrt(2) (complex_minus + complex_plus)
        # real_plus = 1/sqrt(2) (complex_minus - complex_plus)
        # (i real_minus + real_plus) = - sqrt(2) * complex_plus
        # (i real_minus - real_plus) = - sqrt(2) * complex_minus
        sm = (s2 - 1j * s1) / 2.**.5
        sp = (-1j * s1 - s2) / 2.**.5

        testing.assert_allclose(self.__calc_overlap__(orb1, orb2m), sm, atol=1e-4, rtol=1e-3)
        testing.assert_allclose(self.__calc_overlap__(orb1, orb2p), sp, atol=1e-4, rtol=1e-3)

    def test_overlap_sdxydx2y2(self):
        orb1 = 0, 0, 0
        orb2m = 2, -2, 0
        orb2p = 2, 2, 0

        orb_ix1 = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb1, orb2m))
        orb_ix2 = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb1, orb2p))

        s1 = self.h_overlap[self.shift_vector_id, orb_ix1[0], orb_ix1[1]]
        s2 = self.h_overlap[self.shift_vector_id, orb_ix2[0], orb_ix2[1]]

        # Transform real to complex
        # real_minus = i/sqrt(2) (complex_minus - complex_plus)
        # real_plus = 1/sqrt(2) (complex_minus + complex_plus)
        # (i real_minus + real_plus) = sqrt(2) * complex_plus
        # (i real_minus - real_plus) = - sqrt(2) * complex_minus
        sm = (s2 - 1j * s1) / 2.**.5
        sp = (1j * s1 + s2) / 2.**.5

        testing.assert_allclose(self.__calc_overlap__(orb1, orb2m), sm, atol=1e-5, rtol=1e-3)
        testing.assert_allclose(self.__calc_overlap__(orb1, orb2p), sp, atol=1e-5, rtol=1e-3)

    def test_overlap_pzdz2(self):
        orb1 = 1, 0, 0
        orb2 = 2, 0, 0

        orb_ix = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb1, orb2))

        testing.assert_allclose(
            self.__calc_overlap__(orb1, orb2),
            self.h_overlap[self.shift_vector_id, orb_ix[0], orb_ix[1]],
            atol=1e-5, rtol=1e-3,
        )

    def test_overlap_pxydxzdyz(self):
        orb1m = 1, -1, 0
        orb1p = 1, 1, 0
        orb2m = 2, -1, 0
        orb2p = 2, 1, 0

        orb_ix1 = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb1m, orb1p))
        orb_ix2 = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb2m, orb2p))

        s = self.h_overlap[self.shift_vector_id, orb_ix1, :][:, orb_ix2]

        # Transform real to complex
        #    dyz dxz          -1  1
        # py  .   .  ->  -1   .   .
        # pz  .   .       1   .   .
        # real_minus = i/sqrt(2) (complex_minus + complex_plus)
        # real_plus = 1/sqrt(2) (complex_minus - complex_plus)
        # (- i real_minus - real_plus) / sqrt(2) = complex_plus
        # (- i real_minus + real_plus) / sqrt(2) = complex_minus
        transform = np.array([
            [-1.j, 1],
            [-1.j, -1],
        ]) / 2.**.5
        s_cmplx = np.einsum("ij,ai,bj->ab", s, transform.conj(), transform)

        testing.assert_allclose(self.__calc_overlap__(orb1m, orb2m), s_cmplx[0, 0], atol=1e-4, rtol=1e-3)
        testing.assert_allclose(self.__calc_overlap__(orb1p, orb2p), s_cmplx[1, 1], atol=1e-4, rtol=1e-3)
        testing.assert_allclose(self.__calc_overlap__(orb1m, orb2p), s_cmplx[0, 1], atol=1e-4, rtol=1e-3)
        testing.assert_allclose(self.__calc_overlap__(orb1p, orb2m), s_cmplx[1, 0], atol=1e-4, rtol=1e-3)

    def test_overlap_pxydxydx2y2(self):
        orb1m = 1, -1, 0
        orb1p = 1, 1, 0
        orb2m = 2, -2, 0
        orb2p = 2, 2, 0

        orb_ix1 = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb1m, orb1p))
        orb_ix2 = tuple(ravel_orbital_index(*o, n_max=1) for o in (orb2m, orb2p))

        s = self.h_overlap[self.shift_vector_id, orb_ix1, :][:, orb_ix2]

        # Transform real to complex
        #    dxy dx2y2          -2  2
        # py  .   .    ->  -1   .   .
        # pz  .   .         1   .   .
        transform_p = np.array([
            [-1.j, 1],
            [-1.j, -1],
        ]) / 2.**.5
        transform_d = np.array([
            [-1.j, 1],
            [1.j, 1],
        ]) / 2.**.5
        s_cmplx = np.einsum("ij,ai,bj->ab", s, transform_p.conj(), transform_d)

        testing.assert_allclose(self.__calc_overlap__(orb1m, orb2m), s_cmplx[0, 0], atol=1e-4, rtol=1e-3)
        testing.assert_allclose(self.__calc_overlap__(orb1p, orb2p), s_cmplx[1, 1], atol=1e-4, rtol=1e-3)
        testing.assert_allclose(self.__calc_overlap__(orb1m, orb2p), s_cmplx[0, 1], atol=1e-4, rtol=1e-3)
        testing.assert_allclose(self.__calc_overlap__(orb1p, orb2m), s_cmplx[1, 0], atol=1e-4, rtol=1e-3)

    def test_overlap_all(self, ls=(0, 1, 2), atol=1e-4, rtol=1e-4):
        tau, theta, phi = c2s(self.shift_vector @ self.c_vectors)

        n1, n2 = 0, 0

        overlaps = {}

        for l1, l2 in product(ls, repeat=2):
            radial_quads = [q2c_ovlp_radial_quad(
                self.paos_k[l1, n1],
                self.paos_k[l2, n2],
                tau,
                _l,)
                for _l in range(2 * max(l1, l2) + 1)]

            overlap = [[q2c_ovlp(l1, m1, l2, m2, theta, phi, radial_quads)
                for m2 in range(-l2, l2 + 1)] for m1 in range(-l1, l1 + 1)]

            overlaps[l1, l2] = np.array(overlap)

        S = np.block([[overlaps[l1, l2] for l2 in ls] for l1 in ls])

        # transform to real basis
        c2r = la.block_diag(*[c2r_spherical_transform(l) for l in ls])
        S = c2r @ S @ c2r.conj().T

        # transform conventions
        reorder = index_order([0], [(1, 1, 1)])
        S = S[reorder, :][:, reorder]

        mask2 = np.concatenate([[l in ls] * (2 * l + 1) for l in range(3)] + [[False] * 9])
        sref = self.h_overlap[self.shift_vector_id][mask2, :][:, mask2]

        testing.assert_allclose(
            S,
            sref,
            atol=atol,
            rtol=rtol,
        )
